module Adapter
  class Twitter
    def twitt
      puts 'Twitt was published'
    end
  end
  class Facebook
    def post
      puts 'Facebook post was published'
    end
  end
  class TwitterAdapter
    def initialize
      @webservice = Twitter.new
    end

    def send_message
      @webservice.twitt
    end
  end
  class FacebookAdapter
    def initialize
      @webservice = Facebook.new
    end

    def send_message
      @webservice.post
    end
  end
  class Message
    attr_accessor :webservice
    def send
      @webservice.send_message
    end
  end

  def self.run

    message = Message.new

    message.webservice = TwitterAdapter.new
    message.send

    message.webservice = FacebookAdapter.new
    message.send

  end
end

Adapter.run
